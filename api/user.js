const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../config/key');
// +++++ load user model +++++
const Employee = require('../models/Employee');
const config = require('../config/key');
// @routes  GET /api/user/test
// @desc    Test usrs
// @access  public
router.get('/test',(req,res)=>{
    res.json({
        msg : 'user worked'
    })
});

// @routes  POST /api/user/register
// @desc    register user
// @access  public
router.post('/register', (req,res)=>{
    Employee.findOne({ email: req.body.email}).then(employee =>{
        if(employee){            
            return res.status(400).json({ email: 'Email already exist'});
        }
        else{                    
            const employee = new Employee({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
            })
            bcrypt.genSalt(10, (err,salt) =>{
                bcrypt.hash(employee.password,salt,(err,hash)=>{
                    if(err) throw err;
                        employee.password = hash;
                        employee.save().then(
                        employee => res.json(employee)
                    ).catch(err => console.log(err));
                    })
                })
        }
});
}); 
   
// @routes  GET /api/user/login
// @desc  login user
// @access  public
router.post('/login', (req,res) => {
    const { email, password } = req.body;
    // check user email
    Employee.findOne({email}).then(employee =>{
    if(!employee){
        return res.status(404).json({email:'incorrect email id'});
    }
    else{
        // check password
        bcrypt.compare(password, employee.password).then(isMatch =>{
            if (isMatch){
        //  JWT Token 
            const payload = { id: employee.id , name: employee.name }; //create jwt payload
        //  Sign Token
            jwt.sign(
                payload,
                keys.secret,
                { expiresIn: 3600 },
                (err, token) => {
                    res.json({
                        success: true,
                        token: token,
                        msg: 'login successful'
                    });
                });
            }else{
                return res.status(400).json({password: 'incorrect password'});
            }
        });
    }
   
   });
});

// @routes  GET /api/user/fetch
// @desc  fetch data from database
// @access  public
router.post('/fetch',(req,res) => { 
        Employee.find({email: req.body.email}).then(employee =>{
            if(employee){
                console.log(employee);}
           
     });
 });
// @routes  GET /api/user/test/id
// @desc  id 
// @access  public
router.get('/test/:id',(req,res) => {
      res.send('id'+ req.params.id)
});  

// @routes  GET /api/user/current
// @desc    return Current employee
// @access  private
router.get('/current',(req,res) => {
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
    jwt.verify(token, config.secret, function(err, decoded) {
        if (err){
            return res.status(500).send({
                 auth: false, 
                 message: 'Failed to authenticate token.' 
                });
        } 
        else{
            res.status(200).send(decoded);
        }
      });
});
 
module.exports = router;