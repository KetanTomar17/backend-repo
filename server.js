const express = require('express');
const mongoose = require('mongoose');
const app = express();
const passport = require('passport');
// load bodyparser
const bodyparser = require('body-parser');

// +++++ Routes files +++++
const user = require('./api/user');
const profile = require('./api/profile');
const posts = require('./api/posts');

// +++++ DB config +++++
const db = require('./config/key').mongoURI;

//body parser middle ware 
app.use(bodyparser.urlencoded({ extended: false}));
app.use(bodyparser.json());
// +++++ DB connect +++++
mongoose.connect(db,{useNewUrlParser: true}).then(()=>{
    console.log('connected');
}).catch((err)=>{
    console.log(err);
})

/*app.get('/', (req,res) => {
    res.send('Welcome);
});*/

//passport middleware
app.use(passport.initialize())

//+++++ use routes +++++
app.use('/api/user',user);
app.use('/api/profile',profile);
app.use('/api/posts',posts);

const port = process.env.port || 2000;

app.listen(port , ()=> {
    console.log(`server is running on ${port}`);
});